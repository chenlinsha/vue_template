import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const router = new Router({
  mode: "history",
  scrollBehavior(to) {
    if (to.hash) {
      return {
        selector: to.hash
      };
    }
  },
  routes: [
    {
      path: "*",
      name: "404",
      component: _ => require(["@/views/404"], _),
      meta: {
        title: "抱歉，页面未找到"
      }
    },
    {
      path: "/500",
      name: "500",
      component: _ => require(["@/views/500"], _),
      meta: {
        title: "抱歉，服务器出错了"
      }
    },
    {
      path: "/403",
      name: "403",
      component: _ => require(["@/views/403"], _),
      meta: {
        title: "抱歉，你无权访问该页面"
      }
    },
    {
      path: "/index",
      redirect: "/"
    },
    {
      path: "/",
      name: "Index",
      redirect: "/dynamic-form",
      meta: {
        title: "首页",
        requireAuth: false,
        keepAlive: true
      },
      component: _ => require(["@/views/Index"], _),
      children: [
        {
          path: "/table",
          name: "Table",
          meta: {
            title: "表格",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/table/Index"], _)
        },
        {
          path: "/pagination",
          name: "Pagination",
          meta: {
            title: "分页",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/pagination/Index"], _)
        },
        {
          path: "/rate",
          name: "Rate",
          meta: {
            title: "评分",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/rate/Index"], _)
        },
        {
          path: "/upload",
          name: "Upload",
          meta: {
            title: "上传",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/upload/Index"], _)
        },
        {
          path: "/message",
          name: "Message",
          meta: {
            title: "消息提示",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/message/Index"], _)
        },
        {
          path: "/message-box",
          name: "MessageBox",
          meta: {
            title: "弹框",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/message-box/Index"], _)
        },
        {
          path: "/modal",
          name: "Modal",
          meta: {
            title: "模态框",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/modal/Index"], _)
        },
        {
          path: "/checkbox",
          name: "Checkbox",
          meta: {
            title: "多选",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/checkbox/Index"], _)
        },
        {
          path: "/dates",
          name: "Dates",
          meta: {
            title: "时间",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/dates/Index"], _)
        },
        {
          path: "/dynamic-form",
          name: "DynamicForm",
          meta: {
            title: "动态表单（自定义表单）",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/modules/dynamic-form/Index"], _)
        },
        {
          path: "/form",
          name: "Form",
          meta: {
            title: "自定义表单",
            requireAuth: false,
            keepAlive: false
          },
          component: _ => require(["@/views/Form"], _)
        }
      ]
    }
  ]
});

router.beforeEach((to, from, next) => {
  //通过路由设置标题
  if (typeof to.meta.title !== "undefined") {
    document.title += "-" + to.meta.title;
  }
  next();

  /*
    // 判断是否需要登录权限
    if (to.matched.some(res => res.meta.requireAuth)) {
      const loginUrl=store.state.loginAddress;
      console.log(store.state.loginAddress);
      //alert(loginUrl);
      let url=location.protocol+'//'+location.host;
      //document.location =  loginUrl+"/login?service=" +url+to.fullPath;

      if (store.state.loginFlag) {//store.state.nickName
        next();
      } else {// 没登录则跳转到登录界面
        const loginUrl=store.state.loginAddress;
        let url=location.protocol+'//'+location.host;
        //document.location =  loginUrl+"/login?service=" +url+to.fullPath;
      }
    } else {
      next();
    }*/
});

export default router;
