import UploadImgs from "./UploadImgs.vue";

UploadImgs.install = function(Vue) {
  Vue.component(UploadImgs.name, UploadImgs);
};

export default UploadImgs;
