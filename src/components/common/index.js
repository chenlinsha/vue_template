import Pagination from "@/components/common/pagination";
import Rate from "@/components/common/rate";

import Loading from "@/components/common/loading";
import Message from "@/components/common/message";

import Confirm from "@/components/common/confirm";
import Modal from "@/components/common/modal";
import Table from "@/components/common/table";
import UploadImgs from "@/components/common/upload-imgs";

const components = [
  Pagination,
  Rate,
  UploadImgs,
  Loading,
  Message,
  Confirm,
  Modal,
  Table
];

const install = function(Vue) {
  components.forEach(component => {
    Vue.component(component.name, component);
  });
  Vue.prototype.$my_message = Message.install;
  Vue.prototype.$my_confirm = Confirm.install;
};

export default install;
