#

> Vue 组件练习 (vue-cli3)

## 安装步骤

``` bash
# 安装项目依赖
npm install

# 运行项目
npm run dev

# 打包
npm run serve

```

## Loading 数据加载组件
| 参数名  | 类型  | 说明  |
| ------------ | ------------ | ------------ |
|  loadFlag | Boolean  | 是否显示loading |
|  loadError | Boolean  | 是否加载失败  |
|  data | Array  | 要加载的数据  |
|  @refresh | fn  | 重新加载数据的方法  |

```
<my-loading :loadFlag="loadFlag"
            :loadError="loadError"
            :data="tableList"
            @refresh="getTableData">
  <ul slot="data">
      <li v-for="item in tableList">
        {{item.zhpf}}
      </li>
  </ul>
</my-loading>
```
## Pagination 分页组件
| 参数名  | 必填  | 类型  | 说明  |
| ------------ | ------------ | ------------ | ------------ |
|  currentPage |  否  | Number  | 当前页， 默认1|
|  limit |  否  | Number  | 每页显示条数，默认10  |
|  totalCount |  是  | Number  | 总条数 |
|  @turn |  是  | Fn  | 翻页刷新数据 |

```
 <my-pagination :currentPage.sync="currentPage"
                :limit.sync="limit"
                :totalCount="totalCount"
                @turn="getTableData"></my-pagination>
```



## UploadImgs 上传图片组件
| 参数名  | 类型  | 说明  |
| ------------ | ------------ | ------------ |
|  fileTypes | Array  | 文件类型， 默认'jpeg','bmp','gif','jpg'|
|  limit | Number  | 限制数量，默认5  |
|  size | Number  | 最大图片大小，默认5M  |
|  @imgs | Object  | 上传的图片文件  |

```
<my-upload-imgs
        :fileTypes="['jpeg','bmp','gif','jpg','png']"
        :limit="4"
        @imgs="val=>imgs=val" />
```

## Rate 星星评分组件
| 参数名  | 类型  | 说明  |
| ------------ | ------------ | ------------ |
| score  | Number  | 分数 ，默认0，保留一位小数 |
|  disabled | Boolean  | 是否只读，默认false，鼠标点击可以打分  |
|  showText | Boolean  | 是否显示分数，默认false  |

```
//只读，不显示数字
<my-rate :score="1.5" disabled/>
//只读，显示数字：
<my-rate :score="3.6" disabled showText/>
//鼠标点击评分,显示数字：
<my-rate :score.sync="curScore" showText/>
<button @click="submit">提交</button>
//submit(){alert(this.curScore);} 提交显示分数
```

## Message 消息组件
| 参数名  | 类型  | 说明  |
| ------------ | ------------ | ------------ |
| content  | String  | 内容 |
|  time | Number  | 消失时间，默认3秒后消失  |
|  type | String  | info/success/warning/error，默认info  |
|  hasClose | Boolean  | 是否含关闭按钮，默认false  |
|  top | String  | 消息框位置，默认顶部  |
|  left | String  | 消息框位置，默认居中  |

```
//简写，第一个参数为内容，第二个为类型
this.$my_message('这是一个message');
this.$my_message('这是一个warning','warning');

//传参的方式
this.$my_message({
    content:'这是一个success提示',
    time:5000000,
    type:'success',
    hasClose:true,
});

//改变位置
let e = event || window.event,
  top = e.clientY - 60,
  left = e.clientX - 30;
this.$my_message({
  content: '这是一个warning!',
  type: 'warning',
  top: top + 'px',
  left: left + 'px',
});
```

## Confirm 确认框组件
| 参数名  | 类型  | 说明  |
| ------------ | ------------ | ------------ |
| title  | String  | 标题 |
| content  | String  | 内容 |
|  yesBtnText | String  | 确认按钮文字，默认‘确定’  |
|  cancelBtnText | String  | 取消按钮文字，默认‘取消’  |
|  type | String  | info/success/warning/error，默认‘’  |

```
show(){
	this.$my_confirm('是否登录?',{
		yesBtnText:'登录',
	}).then(() => {
		//点登录
	}).catch(() => {
		//点取消
	});
},
show1(){
	this.$my_confirm('此操作将永久删除该文件, 是否继续？','提示',{
		yesBtnText:'是',
		cancelBtnText:'否',
		type: 'warning'
	}).then(() => {
		//点是
	}).catch(() => {
		//点否
	});
},
```

## Modal 模态框
| 参数名  | 类型  | 说明  |
| ------------ | ------------ | ------------ |
| visible  | Boolean  | 是否显示，默认false |
| title  | String  | 标题 |
| update:visible | Boolean  | 更新visible, 使用`:visible.sync`实现动态绑定 |

```
<my-modal title="消息" :visible.sync="isVisible">
  <div slot="content">
    内容
  </div>
  <div slot="ft" class="ft">
    <a class="btn btn-primary" @click="sure">确定</a>
    <a class="btn btn-default" @click="isVisible=false">取消</a>
  </div>
</my-modal>
```


## Dynamic Form 动态表单（自定义表单）
**showType:** form、table、row
### Form Attributes

| 参数  | showType | 说明  | 类型 | 可选值  | 默认值  | 例 |
| :------------ | :------------ | :------------ | :------------ | :------------ | :------------ | :------------ |
| formDatas  |   | 表单数据  | object  |  - |  - |   |
| editFlag.sync  |   | 表单是否是编辑状态  | boolean  | true/false  | true  |   |
| params  |   | 提交时要传的参数  | object  | -  | -  | :params="{'xh':'111'}"  |
| btnsShow | form | 提交按钮是否显示  | boolean  | true/false  | true  |   |
| cancelBtnShow  | form  | 取消按钮是否显示 | boolean  | true/false |  true |   |
| resetBtnShow  | form  | 重置按钮是否显示 | boolean  | true/false |  false |   |
|addBtnShow|table/row|是否显示添加按钮| boolean  | true/false |  true |   |
| submitText  | form  | 提交按钮文本 | string  | -  | 提交  |   |
| onSuccess  |  form | 提交成功时的钩子 | function(response)  | - | - |   |
| onError  |  form | 提交失败时的钩子 | function(response)  |  - | -  |   |
| onlyEdit |  form | 只显示可编辑的  |boolean  | true/false  | false  |   |


### Form Methods

| 方法名  | showType | 说明  | 参数 | 例 |
| :------------ | :------------ | :------------ | :------------ |
| reloadData  |  | 刷新表单  | -  |   |
| editableDatas  | form | 返回编辑状态的键和值  | Function(props: object）  |  @submitDatas="val=>applyFormDatas=val" |
| validate  |  form | 返回验证结果  | Function(props: boolean）  |   @validate="val=>applyValidate=val" |
|allFormDatas | form |返回表单所有值|Function(props: object）  |  |

```
//执行验证
this.$refs["otherForm"].validateForm();
//验证结果
this.applyValidate;
//编辑类型的表单数据
this.submitDatas；
//重置自定义表单
 this.$refs["applyForm"].resetForm();
```